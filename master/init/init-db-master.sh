#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    CREATE USER replicator WITH REPLICATION ENCRYPTED PASSWORD 'rep_pass';
    CREATE DATABASE rep_db;
    GRANT ALL PRIVILEGES ON DATABASE rep_db TO replicator;
EOSQL

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "rep_db" <<-EOSQL
    CREATE SCHEMA schema_example
      CREATE TABLE example (
  	     id serial PRIMARY KEY,
  	     col1 VARCHAR ( 50 ) UNIQUE NOT NULL,
         col2 VARCHAR ( 50 ) UNIQUE NOT NULL
      );
    INSERT INTO schema_example.example VALUES( 1, 'a', 'd');
    INSERT INTO schema_example.example VALUES( 2, 'b', 'e');
    INSERT INTO schema_example.example VALUES( 3, 'c', 'f');
EOSQL

echo "host replication replicator 0.0.0.0/0 trust" >> /var/lib/postgresql/data/pgdata/pg_hba.conf
