# postgres-replication

- Мастер на локальном порте: 5432
- Слейв на локальном порте: 5433
- Логический публикатор: 5434
- логический консьюмер: 5435

запуск:

1) поднимаем мастера
`cd master && docker-compose up`

Помнимется мастер с заполненной базой и созданным юзером репликации
`commit_delay = 300000` (задержка установлена в 5 минут)

2) поднимаем слейва

`cd slave && docker-compose up`

он сделает бэкап и отвалится, поднимем контейнер еще раз
`docker-compose up`

3) проверяем репликацию
([тык1](https://gitlab.com/dmamontov/postgres-replication/-/raw/master/img/%D0%A1%D0%BD%D0%B8%D0%BC%D0%BE%D0%BA%20%D1%8D%D0%BA%D1%80%D0%B0%D0%BD%D0%B0%202020-12-29%20%D0%B2%2017.25.22.png)) -- мастер
([тык2](https://gitlab.com/dmamontov/postgres-replication/-/raw/master/img/%D0%A1%D0%BD%D0%B8%D0%BC%D0%BE%D0%BA%20%D1%8D%D0%BA%D1%80%D0%B0%D0%BD%D0%B0%202020-12-29%20%D0%B2%2017.28.07.png)) -- слейв
([тык3](https://gitlab.com/dmamontov/postgres-replication/-/raw/master/img/%D0%A1%D0%BD%D0%B8%D0%BC%D0%BE%D0%BA%20%D1%8D%D0%BA%D1%80%D0%B0%D0%BD%D0%B0%202020-12-29%20%D0%B2%2017.37.58.png)) -- слот репликации

4) поднимаем logic-produser и проверяем слот публикации ([тык4](https://gitlab.com/dmamontov/postgres-replication/-/raw/master/img/%D0%A1%D0%BD%D0%B8%D0%BC%D0%BE%D0%BA%20%D1%8D%D0%BA%D1%80%D0%B0%D0%BD%D0%B0%202020-12-29%20%D0%B2%2018.27.05.png))

`cd ../logic-produser && docker-compose up`

5) поднимаем консумера и делаем инсерт ([тык](https://gitlab.com/dmamontov/postgres-replication/-/raw/master/img/%D0%A1%D0%BD%D0%B8%D0%BC%D0%BE%D0%BA%20%D1%8D%D0%BA%D1%80%D0%B0%D0%BD%D0%B0%202020-12-29%20%D0%B2%2019.11.22.png))

`cd ../logic-consumer && docker-compose up`
