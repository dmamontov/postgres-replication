#!/bin/bash
set -e

rm -rf $PGDATA/*
pg_basebackup -h master -U replicator -p 5432  -D $PGDATA -Fp -Xs -P -R --slot otus -C
